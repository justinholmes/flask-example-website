from flask import render_template

from flask_example_website import app
from flask_example_website.models.code import Code


@app.route("/")
def index():
    all_code = Code.query.all()
    return render_template('index.html', code=all_code)
