from flask_example_website import db


class Code(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    sql = db.Column(db.Text, nullable=False)
    spark = db.Column(db.Text, nullable=False)
    kdbq = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return '<Code %r>' % self.name
