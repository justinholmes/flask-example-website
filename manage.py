from flask_migrate import MigrateCommand
from flask_script import Server, Manager
from flask_example_website import app

manager = Manager(app)
manager.add_command("runserver", Server(use_reloader=True))
manager.add_command('db', MigrateCommand)

if __name__ == "__main__":
    manager.run()
