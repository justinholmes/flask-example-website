# example flask website

This shows how to wire up Flask, flask_sqlalchemy, flask-migrate, flask-bootstrap, flask_script using Pipenv

### SQL migrations

SQL migrations use https://flask-migrate.readthedocs.io/en/latest/

Run these after checking out the code for the first time


```$ python manage.py db init```

```$ python manage.py db migrate```

```$ python manage.py db upgrade```

### Run DEV server

```python manage.py runserver```

